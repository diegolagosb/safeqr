import React, { useState } from "react";
import {
  StyleSheet,
  View,
  StatusBar,
  Text,
  TextInput,
  TouchableOpacity,
  Alert
} from "react-native";
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome";
import InputPasswordToggle from 'react-native-toggle-password-visibility';

export default function Login(props) {
  const [password, setPassword] = useState('');
  return (
    <View style={styles.main}>
      <StatusBar backgroundColor="rgba(40,171,185,1)" />
      <View style={{ flex: 1, width: '100%', maxHeight: 10 }}></View>
      <View style={styles.loginTextRow}>
        <Text style={styles.loginText}>INGRESA CON {"\n"}TU CUENTA</Text>
      </View>
      <View style={{ flex: 1, width: '100%', maxHeight: 20 }}></View>
      <View style={styles.iconLoginRow}>
        <FontAwesomeIcon
          name="user-circle-o"
          style={styles.icon}
        ></FontAwesomeIcon>
      </View>
      <View style={{ flex: 1, width: '100%', maxHeight: 20 }}></View>
      <View style={styles.groupInputs}>
        <View style={styles.inputRut}>
          <View style={styles.rutContainer}>
            <View style={styles.rutIconRow}>
              <FontAwesomeIcon
                name="id-card"
                style={styles.rutIcon}
              ></FontAwesomeIcon>
              <TextInput
                placeholder="RUT"
                placeholderTextColor="rgba(40,171,185,1)"
                keyboardType="numeric"
                maxLength={9}
                returnKeyType="done"
                style={styles.rut}
              ></TextInput>
            </View>
          </View>
        </View>
        <View style={styles.inputPassword}>
          <View style={styles.passwordContainer}>
            <View style={styles.passwordIconRow}>
              <FontAwesomeIcon
                name="key"
                style={styles.passwordIcon}
              ></FontAwesomeIcon>
              <InputPasswordToggle
                value={password}
                onChangeText={setPassword}
                placeholder="CONTRASEÑA"
                placeholderTextColor="rgba(40,171,185,1)"
                style={styles.passwordVisibleIcon} />
            </View>
          </View>
        </View>
      </View>
      <View style={styles.rowPrincipalBtn}>
        <TouchableOpacity
          onPress={() => props.navigation.navigate("Home")}
          style={styles.principalBtn}
        >
          <Text style={styles.iniciarSesion}>
            INICIAR SESIÓN
            </Text>
        </TouchableOpacity>
      </View>
      <View style={{ flex: 1, width: '100%', maxHeight: 10 }}></View>
    </View>
  );
}

const styles = StyleSheet.create({
  main: {
    backgroundColor: "rgba(40,171,185,1)",
    flex: 1,
    flexDirection: "column"
  },
  loginTextRow: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    maxHeight: 100,
    marginTop: 50
  },
  loginText: {
    fontFamily: "balsamiq-sans-700",
    color: "rgba(255,255,255,1)",
    fontSize: 30,
    textAlign: "center"
  },
  iconLoginRow: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    maxHeight: 130
  },
  icon: {
    color: "rgba(255,255,255,1)",
    fontSize: 100,
    height: 100,
    width: 100
  },
  groupInputs: {
    flex: 1,
    flexDirection: "column",
    width: '100%',
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    padding: 30,
    maxHeight: 200
  },
  rutContainer: {
    flex: 1,
    backgroundColor: "rgba(255,255,255,1)",
    width: '100%',
    flexDirection: "row"
  },
  rutIcon: {
    fontSize: 25,
    color: "rgba(40,171,185,1)",
    height: 25,
    width: 28,
    marginTop: 7
  },
  inputRut: {
    width: '100%',
    height: 59,
    marginBottom: 30
  },
  rut: {
    fontFamily: "balsamiq-sans-700",
    color: "rgba(40,171,185,1)",
    width: '100%',
    height: 42,
    marginLeft: 9
  },
  rutIconRow: {
    height: 42,
    flexDirection: "row",
    flex: 1,
    marginRight: 12,
    marginLeft: 9,
    marginTop: 10
  },
  inputPassword: {
    width: '100%',
    height: 56
  },
  passwordContainer: {
    flex: 1,
    backgroundColor: "rgba(255,255,255,1)",
    width: '100%',
    flexDirection: "row"
  },
  passwordIcon: {
    color: "rgba(40,171,185,1)",
    fontSize: 25,
    height: 25,
    width: 25,
    marginTop: 6
  },
  passwordVisibleIcon: {
    fontFamily: "balsamiq-sans-700",
    fontWeight: "bold",
    color: "rgba(40,171,185,1)",
    height: 25,
    width: '85%',
    marginTop: 6,
    marginLeft: 12
  },
  passwordIconRow: {
    flexDirection: "row",
    flex: 1,
    marginRight: 13,
    marginLeft: 12,
    marginTop: 9
  },
  rowPrincipalBtn: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    maxHeight: 100
  },
  principalBtn: {
    backgroundColor: 'white',
    width: 200,
    borderRadius: 30,
    padding: 10
  },
  iniciarSesion: {
    fontFamily: "balsamiq-sans-700",
    color: "rgba(40,171,185,1)",
    fontSize: 20,
    textAlign: "center",
    alignSelf: "center"
  }
});