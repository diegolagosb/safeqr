import React, { Component } from "react";
import { StyleSheet, View } from "react-native";

function Register(props) {
  return (
    <View style={styles.container}>
      <View style={styles.main}></View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  main: {
    backgroundColor: "rgba(40,171,185,1)",
    width: 375,
    flex: 1,
    alignSelf: "center"
  }
});

export default Register;
