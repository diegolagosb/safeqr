import React, { Component } from "react";
import { StyleSheet, View } from "react-native";

function Home(props) {
  return (
    <View style={styles.container}>
      <View style={styles.main}></View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center"
  },
  main: {
    backgroundColor: "rgba(40,171,185,1)",
    width: 375,
    height: 812,
    alignSelf: "center"
  }
});

export default Home;
