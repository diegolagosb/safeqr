import React, { Component } from "react";
import {
  StyleSheet,
  View,
  StatusBar,
  Text,
  TouchableOpacity,
  Image
} from "react-native";

export default function Start(props) {
  return (
    <View style={styles.main}>
      <StatusBar backgroundColor="rgba(40,171,185,1)" />
      <View style={styles.bienvenidoASafeqrColumn}>
        <Text style={styles.bienvenidoASafeqr}>
          BIENVENIDO/A {"\n"}A SAFEQR
          </Text>
      </View>
      <View style={styles.imageContainer}>
        <Image
          source={require('./../assets/images/servicio-medico_24908-41290.jpg')}
          resizeMode="contain"
          style={styles.image}
        ></Image>
      </View>
      <View style={styles.footer}>
        <View style={styles.rowPrincipalBtn}>
          <TouchableOpacity
            onPress={() => props.navigation.navigate("Login")}
            style={styles.principalBtn}
          >
            <Text style={styles.iniciarSesion}>
              INICIAR SESIÓN
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.linkRegisterRow}>
          <Text style={styles.noTienesCuenta}>¿No tienes cuenta?</Text>
          <View style={styles.registerLink}>
            <TouchableOpacity
              onPress={() => props.navigation.navigate("Register")}
              style={styles.button}
            >
              <Text style={styles.registrate}>Regístrate</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={{ flex: 1, width: '100%' }}></View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  main: {
    backgroundColor: "rgba(40,171,185,1)",
    flex: 1,
    flexDirection: "column"
  },
  bienvenidoASafeqrColumn: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    width: '100%',
  },
  bienvenidoASafeqr: {
    fontFamily: "balsamiq-sans-700",
    color: "rgba(255,255,255,1)",
    textAlign: "center",
    fontSize: 42,
    width: 375,
    height: 101
  },
  imageContainer: {
    maxHeight: 350,
    justifyContent: "center",
    alignItems: "center"
  },
  image: {
    maxHeight: 300
  },
  footer: {
    flex: 1,
    flexDirection: "column",
    alignSelf: "center"
  },
  rowPrincipalBtn: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  principalBtn: {
    backgroundColor: 'white',
    width: 200,
    borderRadius: 30,
    padding: 10
  },
  iniciarSesion: {
    fontFamily: "balsamiq-sans-700",
    color: "rgba(40,171,185,1)",
    fontSize: 20,
    textAlign: "center",
    alignSelf: "center"
  },
  linkRegisterRow: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    height: 41,
    width: 293
  },
  noTienesCuenta: {
    fontFamily: "balsamiq-sans-regular",
    color: "rgba(255,255,255,1)"
  },
  registerLink: {
    width: 91,
    height: 29
  },
  button: {
    width: 91,
    height: 29
  },
  registrate: {
    fontFamily: "balsamiq-sans-700",
    color: "rgba(45,97,135,1)",
    marginTop: 3,
    marginLeft: 14
  }
});