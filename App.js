import React, { useState } from "react";
import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import { createDrawerNavigator } from "react-navigation-drawer";
import { AppLoading } from "expo";
import * as Font from "expo-font";
import Home from "./src/screens/Home";
import Start from "./src/screens/Start";
import Login from "./src/screens/Login";
import Register from "./src/screens/Register";

const DrawerNavigation = createDrawerNavigator({
  Start: Start,
  Home: Home,
  Login: Login,
  Register: Register
});

const StackNavigation = createStackNavigator(
  {
    DrawerNavigation: {
      screen: DrawerNavigation
    },
    Start: Start,
    Home: Home,
    Login: Login,
    Register: Register
  },
  {
    headerMode: "none"
  }
);

const AppContainer = createAppContainer(StackNavigation);

function App() {
  const [isLoadingComplete, setLoadingComplete] = useState(false);
  if (!isLoadingComplete) {
    return (
      <AppLoading
        startAsync={loadResourcesAsync}
        onError={handleLoadingError}
        onFinish={() => handleFinishLoading(setLoadingComplete)}
      />
    );
  } else {
    return isLoadingComplete ? <AppContainer /> : <AppLoading />;
  }
}
async function loadResourcesAsync() {
  await Promise.all([
    Font.loadAsync({
      "balsamiq-sans-regular": require("./src/assets/fonts/balsamiq-sans-regular.ttf"),
      "balsamiq-sans-700": require("./src/assets/fonts/balsamiq-sans-700.ttf")
    })
  ]);
}
function handleLoadingError(error) {
  console.warn(error);
}

function handleFinishLoading(setLoadingComplete) {
  setLoadingComplete(true);
}

export default App;
